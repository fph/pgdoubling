function [can,M] = skeleton_symmetric(A,r,n,tau,maxit)
% skeleton approximation of a symmetric definite matrix
%
% [can,M] = skeleton_symmetric(A, r);
% [can,M] = skeleton_symmetric(A, r, [], tau, maxit);
% [can,M] = skeleton_symmetric(Afun, r, n, tau, maxit);
%
% Computes a symmetric variant of the skeleton (or CUR) approximation in [Goreinov, Tyrtyshnikov,
% Zamarashkin, LAA 97] and [Goreinov et al, "How to find a good submatrix",
% 2010]. Namely, we find an index set I with |I|=r such that A \approx VMV^T,
% where M = A(I,I) is an rxr matrix and V is a nxr matrix such that V(I,:)
% is the identity matrix and all its other entries satisfy |V_{ij}|<=tau.
% In particular, V*M = A(:,I). Alternatively, we can write the
% approximation as A \approx A(:,I)*inv(A(I,I))*A(:,I)^T.
%
% The default value for the threshold tau is 2.
%
% The algorithm works with either a nxn matrix A or a function handle Afun.
% If a function handle is given, the dimension n of the matrix has to be
% specified explicitly.
%
% The algorithm uses a randomized initial value, so it may return different
% outputs if it is called multiple times (unless the default random stream
% is set consistently).
%
% The algorithm does at most maxit iterations before failing. We haven't
% encountered failures in cases in which A is (positive or negative)
% definite. The default value of maxit should be ok in almost all cases.
%
% The quantity max(M) approximates max(A). (see theory in their papers).
%
% The function returns a canBasis such that V = subspaceFromCanBasis(can)
% and M = A(I,I). The index set I can be recovered as can.p(1:r).

if not(exist('tau','var')) || isempty(tau)
    tau = 2;
end

if not(exist('maxit','var')) || isempty(maxit)
    maxit = 100;
end

switch class(A)
    case {'double','single'}
        if exist('n','var') && ~isempty(n) && n~= length(A)
            error('Wrong value of n specified');
        end
        % what follows is a half-assed test to see if A is Hermitian.
        % ishermitian() has been introduced in Matlab 2014a, so we cannot
        % use it without breaking compatibility with older versions.
        if size(A,1)~=size(A,2)
            error('A must be square and Hermitian'); 
        end        
        n = size(A,2);
%        if n<11
%            symtest = A;
%        else
%            symtest = A(1:10,1:10);            
%        end
%        if any(any(symtest'~=symtest))
%            error('A must be Hermitian');
%        end
        
        isfun = false;
    case 'function_handle'
        isfun = true;
        % do nothing
    otherwise
        error('A must either be a matrix or a function handle');
end

if isfun
    U = A(randn(n,r));
else
    U = A*randn(n,r);
end
can = canBasisFromSubspace(U, 'threshold', tau); %this is just to get a good initial I
I = nan(1,r);
for iter = 1:maxit
    I = can.p(1:r);
    if isfun
        U = zeros(n,r);
        U(I,:) = eye(r);
        U = A(U);
    else
        U = A(:,I);
    end
    can = canBasisFromSubspace(full(U), 'initialPermutation', can.p, 'threshold', tau);
    if all(I == can.p(1:r))
        break
    end
end
if iter == maxit
    error('Too many iterations taken. Most likely, your matrix is not definite');
end
M = U(I,:);
