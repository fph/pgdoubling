reset(RandStream.getGlobalStream);
n = 15;
r = 4;

for trie = 1:20
    A = gallery('randsvd',n,-1e6,3);
    [can, M] = skeleton_symmetric(A,r,[],1.5);
    V = subspaceFromCanBasis(can);
    I = can.p(1:r);
    assertEqual(M,A(I,I));
    assertVectorsAlmostEqual(V*M,A(:,I));
end

% test the sparse case
for trie = 1:4
    A = sprandsym(4*n, 0.1, 1e-6, 1);
    [can, M] = skeleton_symmetric(A,r);
    V = subspaceFromCanBasis(can);
    I = can.p(1:r);
    assertEqual(M,A(I,I));
    assertVectorsAlmostEqual(V*M,A(:,I));
end



% test the function handle case
for trie = 1:4
    A = gallery('randsvd',n,-1e6,3);
    [can, M] = skeleton_symmetric(@(x) A*x,r,n);
    V = subspaceFromCanBasis(can);
    I = can.p(1:r);
    assertEqual(M,A(I,I));
    assertVectorsAlmostEqual(V*M,A(:,I));
end

% test that the right matrix is reconstructed if it has rank r. Also test
% negative definite case.
for trie = 1:20
    A = randn(n,r); A = -A*A';
    A = A + A'; %to make sure that the matrix is symmetric in exact arithmetic
    [can, M] = skeleton_symmetric(A,r);
    V = subspaceFromCanBasis(can);
    I = can.p(1:r);
    assertEqual(M,A(I,I));
    assertVectorsAlmostEqual(V*M,A(:,I));
    assertVectorsAlmostEqual(V*M*V',A);
end
